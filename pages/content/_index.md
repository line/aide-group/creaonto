---
title: #CreaCube Ontology Documentation
subtitle: A symbolic description of a creative problem-solving task
---
# The #Creacube ontology
## A symbolic description of a creative problem-solving task

We introduce the idea of a symbolic description of a complex human learning task, in order to contribute to better understand how we develop 21st century skills such as creativity, problem solving and computational thinking. The learner is modeled on the basis of knowledge from learning sciences with the contribution of cognitive neurosciences, in the precise context of a task named \#CreaCube, which consists of building an autonomous vehicle using modular robotic cubes.

Such a specification allows for a better communication between the summoned research fields, namely learning science, cognitive neuroscience and computational modeling. Beyond this specification purpose, we suggest performing inferences using available reasoners to better guide the analysis of the observables collected during the experiments. This operationalization of a creative problem-solving activity is part of an exploratory research action.

We focus on a specific learning task instead of considering human learning at a general level to offer a precise and operational formalization
that is translatable to other tasks. We target problem-solving activities using tangible interfaces for computational thinking initiation, especially tasks using an ill-defined formulation appealing to creativity.

The production of this ontology is shared as a free and open resource [here](https://gitlab.inria.fr/line/aide-group/creaonto/-/blob/master/creacube.owl).

### The #Creacube ontology at a glance

This figure shows the main classes and properties used in the #CreaCube ontology:

{{< figure src="/img/v200ontology.png" alt="#CreaCube ontology graph" >}}

The ontology has been recently aligned on the [DOLCE+DnS Ultralite (DUL) ontology](http://ontologydesignpatterns.org/wiki/Ontology:DOLCE+DnS_Ultralite).\
Some of the concepts are also related to the [Cognitive Atlas](http://cognitiveatlas.org/) and the [Affordance Network](http://theaffordances.appspot.com/getaffordances).\
The complete index of the entities is available [here](entities).

### The #CreaCube task in motion

{{< video mp4="/vid/creacube.mp4" title="#CreaCube in motion" >}}

### Reports and papers

We explain our implementation choices in the following papers:

Lisa Roux, Margarida Romero, Frédéric Alexandre, Thierry Viéville, and Chloé Mercier. *Développement d’une ontologie pour l’analyse d’observables de l’apprenant dans le contexte d’une tâche avec des robots modulaires.* Report. Inria, November 2020. https://hal.inria.fr/hal-03013685.

Chloé Mercier, Lisa Roux, Margarida Romero, Frédéric Alexandre, and Thierry Viéville. *Formalizing Problem Solving in Computational Thinking : An Ontology Approach.* In 2021 IEEE International Conference on Development and Learning (ICDL), 1–8. Beijing, China, August 2021. https://doi.org/10.1109/ICDL49984.2021.9515660 and https://hal.inria.fr/hal-03324136.

Supplementary material available [here](docs/ICDL21_supplementary.pdf).\
Video of the talk: https://youtu.be/ic4rk0C4xgA \
Slides and transcript of the video:

{{% slides_transcript src="docs/ICDL21_slides.pdf" name="ICDL21_slides" title="ICDL'2021 presentation" id="icdl21" %}}

'Formalizing Problem Solving in Computer Thinking: an ontology approach'.

To begin with, what exactly do we mean by Computational Thinking?
It is a form of thinking that uses methods commonly found in computer science such as pattern recognition, abstraction, decomposition or algorithms. Computational Thinking is closely related to Problem Solving. One can identify the variables involved in a problem by decomposing it and come up with an algorithmic solution that can be generalized to solve other problems similar to the initial one. In addition to Computational Thinking, problem solving may also require creativity. This is especially the case with ill-defined problems, where the problem space  is not well specified and a creative approach needs to be taken.

These transversal skills are sometimes referred to as 21st century skills, and it is becoming more and more important to adress them in education. For that purpose, understanding how we learn and how we solve problems is a key issue. As a step towards this direction, we propose to model a problem-solving learning activity involving modular robotic artefacts.

The task we study is the following:
The participant is invited to build an autonomous vehicule, composed of 4 items, able to move autonomously from a starting point to an end point. 4 robotic cubelets are provided to complete the task.

The task is relatively easy in the sense that most people come up with a solution in less than 15 minutes, but complex to model because of the large problem space and the lack of specification: the guideline is deliberately vague and possible actions are not clearly stated, appealing for the learner's creativity of interpretation. The teacher supervising the experiment does not provide any help, but the aforementioned instructions are recorded and may be replayed anytime during the activity.

The participant is not supposed to have any prior experience of the modular robotic cubelets.
Therefore, there is a need for exploratory orientation, defined from a learning science perspective, as a way to gather information from the environment: the participant is expected to observe and manipulate the cubes in order to understand their characteristics. This exploration process, combined with by the mobilization and exploitation of prior knowledge, should allow to formulate hypotheses on the material and test these hypotheses, thus defining sub-goals that will help to move towards a solution. It is worth noting that this solution is not unique, as several cubelet configurations may satisfy the guideline.

Let's take a closer look at the material used in this task.
There are two way points and 4 cubelets: a white cubelet, a red one, a black one and a dark blue one. (no, that one is not black)
These cubelets have different colors and features. For example, the white cubelet has wheels.
Features sometimes suggest which actions could possibly be performed with the cubes: such a potential of action is called an affordance. Wheels, for instance, can usually be rolled onto a surface in order to make something move.

As designers of the task, we have certain knowledge about the material. Organizing this knowledge into a structured network, as we are doing right now, is in fact building an ontology. We distinguish concepts or classes from real-world objects called individuals that instantiate those classes. We also define properties to label the relationships between all these resources. sometimes we may also have to add human-readable comments to precise the semantics and clear up any ambiguity. 
Each fact can be stated as a triple (subject predicate object) where the predicate is a property describing the relationship between a subject resource and an object resource. A step further, we may even specify the domain of a given property, that is to say, the class of resources described by this property. Similarly, the range is the class of resources used as value for that property.

Common standards in use for ontology modelling are the Resource Description Framework (RDF) completed by the Web Ontology Language (OWL). In this example, we are using the Turtle syntax to enunciate RDF triples.

How is this formalism helpful?
First, it can be used to share knowledge in a reusable modular way. It facilitates communication between different fields of expertise, in our case, education and learning sciences, cognitive sciences and computer science. Since the information under this form can also be processed by a computing agent, t helps not only with human-to-human communication but human-to mahcine or machien to machine/.

Designing such an ontology thus forces us to accurately specify the task at the material level, as we have seen with the previous example. But since we want to understand how humans learn, we would also like to specify a behavioral and cognitive model of a human engaged in this task, based on previous research in the cognitive neuroscience literature. In that sense, this ontology could be viewed as an epistemological tool encompassing the concepts developed in such models.

There are several points that we want to focus on when describing the learner. As we mentioned earlier, the participant is expected to explore their environment in a chase for affordances that will help them decide what actions to take. These affordances are comprehended with regard to the previous experiences of the learner, which consitutes prior knowledge that can be used to form hypotheses. For example, the learner may need to have some prior experience with wheels to understand that they can be used to make the cube move. the ontological formalism is particuliarly suited to model the logical interdependencies between these pieces of knowledge and hypotheses.
Another point we are interested in developing is the mode of behavior driving the attentional focus. The learner may define goals and subgoals derived from their hypotheses in order to complete the task and increase their knowledge about the environment. But stimuli may also distract them from their initial goal and make them revise their strategy. We want to characterize these two approaches: goal-driven or top-down, versus stimulus-driven or bottom-up.

Moreover, there might be a conflict between the will to complete the task as quickly as possible and the wish to understand as much as possible about the task environment. These concurrent goals are referred to as performance goal and mastery goal in learning sciences.

To better specify the goals involved in the learner behavior, we used a cognitive model that decomposes the goals in 4 cognitive questionings: What and why descrive the goal and the motivation behind it while Where and how refer to the physical means to reach it in link with the sensorimotor pole.

Here are the main classes used in the learner model with the properties that describe the relationships between them and make the link with classes from the material model. We can recognize property cyclic paths corresponding to the behavioral modes we described earlier: the stimulus-driven behavior and the goal driven beahvior.

In the final result of the ontology, we ended up with 75 classes, 40 properties and close to 400 individuals. This figure is only a glance at the main classes and properties, but the complete version is publicly available on our website.

Now let's go back to the concrete applications for this ontology. We already mentioned the epistemological interest, but we do have something more in mind. We would like to use this to make sense of the data that we are currently collecting on the Creacube task particpants. Reasoning systems such as Hermit or Pellet can perform reasoning on data and could therefore infer the learner motives based on the collected observables and the model descibed in the ontology. By doing that, it could also detect inconsistencies between the model and the data if the model is not suited enough to interpret them.
To make data processing easier, we have attributed identifiers for observables of interest and annotated videos of subjects performing the task. Each video is thus converted as a structure data sequence in a machine readable format such as JSON. The experiments and the annotation process are still in progress, but we already have around a hundred annotated videos for children between middle school and the end of elementary school that we target for educational purposes.

The next step is to query the ontological knowledge base against this data. To do this, we can use the query language SPARQL combined with a reasoner such as Pellet.

Our approach still needs further work to bridge the remaining gap between the ontology and the videos. However, this work seems promising and completes the picture of educational ontologies which as of now are mostly used to design learning assistants or describe learning resources. Despite being at a very preliminary stage, our approach thus constitutes an original take on ontology use in education.

{{% /slides_transcript %}}

### The AIDE (Artificial Intelligence Devoted to Education) project

This work is part of a project aiming to explore to what extent cognitive neuroscience, linked to machine learning and knowledge representation, could help to better formalize human learning as studied in educational sciences. Please refer to our [project team website](https://team.inria.fr/mnemosyne/aide/) for a more general introduction to our work.
