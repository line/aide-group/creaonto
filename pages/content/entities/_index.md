---
title: OWL docs
subtitle: Documentation of all classes, properties and individuals
type: page
layout: entities
---

## \#CreaCube Ontology v2.0.0 (latest)

The CreaCube Ontology consists of:

- a detailed model of the material used for the tasks, namely the cubes ([Cubelets®](https://modrobotics.com/)) with their physical features (as provided by the manufacturer) and possible configurations that can be built by assembling them.

- a model of the behavior of a learner engaged in this task, including possible actions as well as the knowledge that may be activated to solve the problem.

The complete index of the entities for each part is available below.

<!-- 
### Entity index

{{< iframe src="latest/creamtrl/index-en.html" name="content" title="Content" id="owldoc">}}

{{< iframe src="latest/creabhvr/index-en.html" name="content" title="Content" id="owldoc">}}
 -->
### Brief changelog

v2.0.0 (2022): Major revision with alignment on DOLCE\
v1.1.0 (2021): First corrections by Chloé Mercier\
v1.0.0 (2020): First version by Lisa Roux