""" 
edit_owldoc.py
--------------
This script is used to make a few changes to the HTML documentation generated automatically by the OWLDoc plugin, 
mostly to improve the display and remove 404 errors.
Install the required dependencies provided by the Pipfile:
`pipenv install` 
and activate the resulting environment:
`pipenv shell`
before running the script
`py edit_owldoc.py`
"""


import os
import shutil
from bs4 import BeautifulSoup

src_folder = r'../owldoc/'
dst_folder = r'../pages/static/entities/'

src = r'../pages/static/css/owldoc.css'
dst =  dst_folder + r'css/owldoc.css'

if os.path.exists(dst_folder):
    shutil.rmtree(dst_folder)
shutil.copytree(src_folder, dst_folder)
shutil.copyfile(src, dst)

for subdir, dirs, files in os.walk(dst_folder):
    
    for filename in files:
        filepath = subdir + os.sep + filename

        if filename.startswith("creacube"):
            shutil.copy(filepath, subdir + os.sep + "index-creacube.html")

for subdir, dirs, files in os.walk(dst_folder):

    for filename in files:
        filepath = subdir + os.sep + filename

        if filepath.endswith(".html"):
            print("Processing: " + filepath)

            with open(filepath, "r", encoding='utf-8') as fp:
                content = fp.read()
                soup = BeautifulSoup(content,features="html.parser")

                head = soup.head
                if head is not None:
                    if all("owldoc.css" not in link.get("href") for link in head.find_all("link")):
                        original_tag = head.find("link", type='text/css')
                        if original_tag is not None and "default.css" in original_tag.get("href"):
                            new_tag = soup.new_tag("link", rel='stylesheet', href=original_tag.get("href").replace("default.css", "owldoc.css"), type='text/css')
                            original_tag.append(new_tag)

                tabs = soup.find(id="tabs")
                if tabs is not None:
                    for a in tabs.find_all("a", href=True):
                        if "Cloud" in a.string:
                            # or "Ontologies" in a.string:
                            a.extract()
                        a["href"] = a.get("href").replace("index.html", "index-creacube.html")


                if "creacube" in filename:

                    for h3 in soup.find_all("h3"):
                        if h3.string and h3.string.startswith("Loaded"):
                            h3.extract()
                    creacube = soup.find(id="creacube")
                    if creacube:
                        creacube.extract()

                with open(filepath, "wb") as fp_output:
                    fp_output.write(soup.prettify("utf-8"))
                    fp_output.close()
                fp.close()