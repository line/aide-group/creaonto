# The #Creacube ontology: a symbolic description of a creative problem-solving task

We introduce the idea of a symbolic description of a complex human learning task, in order to contribute to better understand how we learn. The learner is modeled on the basis of knowledge from learning sciences with the contribution of cognitive neurosciences, including machine learning formalism, in the very precise framework of a task, named \#CreaCube reviewed here, related to initiation to computational thinking presented as an open-ended problem, which involves solving a problem and appealing to creativity.

We target problem-solving tasks using tangible interfaces for computational thinking initiation, and describe in details how we model the task and the learner behavior in this task, including goal-driven versus stimulus-driven behavior and the learner knowledge construction. We show how formalizing these elements using an ontology offers a well-defined computational model and the possibility of inferences about model elements, analyzing and predicting the learner behavior.

The production of this ontology is shared as a free and open resource [here](https://gitlab.inria.fr/line/aide-group/creaonto/-/blob/master/creacube.owl), and more details are provided on https://line.gitlabpages.inria.fr/aide-group/creaonto/ . We explain our implementation choices in the following papers:

Lisa Roux, Margarida Romero, Frédéric Alexandre, Thierry Viéville, and Chloé Mercier. *Développement d’une ontologie pour l’analyse d’observables de l’apprenant dans le contexte d’une tâche avec des robots modulaires.* Report. Inria, November 2020. https://hal.inria.fr/hal-03013685.

Chloé Mercier, Lisa Roux, Margarida Romero, Frederic Alexandre, and Thierry Viéville. *Formalizing Problem-Solving in Computational Thinking : An Ontology Approach.* In ICDL’2021. Beijing, China, August 2021. http://doi/10.1109/ICDL49984.2021.9515660 https://hal.inria.fr/hal-03324136 
